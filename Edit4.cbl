       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Listing9-4.
       AUTHOR. Taktichoke.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 Stars      PIC *****.
       01 NumofStars PIC 9.

       PROCEDURE DIVISION.
       Begin.
           PERFORM VARYING NumofStars FROM 0 BY 1 UNTIL NumofStars > 5 
              COMPUTE Stars = 10 ** (4 - NumofStars )
              INSPECT Stars CONVERTING "10" TO SPACES 
              DISPLAY NumofStars " = " Stars 
           END-PERFORM 
           GOBACK.