       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Exercise-2.
       AUTHOR. Taktichoke Anumon.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 Edit1        PIC ZZZ,999.
       01 Edit2        PIC ZZZ,ZZZ.00. 
       01 Edit3        PIC ZZZ,ZZZ.ZZ.
       01 Edit4        PIC $$,$$9.99.
       01 Edit5        PIC $Z,ZZ9.99.
       01 Edit6        PIC $Z,ZZ9.99CR.
       01 Edit7        PIC -999,999. 
       01 Edit8        PIC +999,999.
       01 Edit9        PIC ++++,++9.
       01 Edit10       PIC 99B99B99.
       01 Edit11       PIC Z(6).00. 
       01 Edit12       PIC ZZZZZZ00.  
       01 Edit13       PIC XBXBXBBXX.  
       01 Edit14       PIC $***,**9.99.  
       01 Edit15       PIC $$,$$9.99.  




       PROCEDURE DIVISION.
       Begin.
           MOVE 000321 TO Edit1 
           DISPLAY "000321   = " Edit1
           MOVE 004321 TO Edit1 
           DISPLAY "004321    = " Edit1 
           MOVE 000004 TO Edit1 
           DISPLAY "000004   = " Edit1 
           MOVE 654321 TO Edit2 
           DISPLAY "654321   = " Edit2 
           MOVE 654321 TO Edit3 
           DISPLAY "654321   = " Edit3 
           MOVE 004321 TO Edit4
           DISPLAY "004321   = " Edit4
           MOVE 000078 TO Edit4 
           DISPLAY "000078   = " Edit4 
           MOVE 000078 TO Edit5 
           DISPLAY "000078   = " Edit5
           MOVE 000078 TO Edit6 
           DISPLAY "000078   = " Edit6
           MOVE -045678 TO Edit6 
           DISPLAY "-045678  = " Edit6 
           MOVE -123456 TO Edit7 
           DISPLAY "-123456  = " Edit7 
           MOVE 123456 TO Edit7 
           DISPLAY "123456   = " Edit7  
           MOVE 123456 TO Edit8 
           DISPLAY "123456   = " Edit8  
           MOVE -123456 TO Edit8 
           DISPLAY "-123456  = " Edit8  
           MOVE 001234 TO Edit9 
           DISPLAY "001234   = " Edit9  
           MOVE 123456 TO Edit10 
           DISPLAY "123456   = " Edit10  
           MOVE 001234 TO Edit11 
           DISPLAY "001234   = " Edit11  
           MOVE 000092 TO Edit12 
           DISPLAY "000092   = " Edit12  
           MOVE "123GO" TO Edit13 
           DISPLAY "123GO    = " Edit13  
           MOVE 000123 TO Edit14 
           DISPLAY "000123   = " Edit14  
           MOVE 24123.45 TO Edit15 
           DISPLAY "24123.45 = " Edit15  
           .
       