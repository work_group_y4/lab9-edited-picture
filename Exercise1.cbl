       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Exercise-1.
       AUTHOR. Taktichoke Anumon.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 Edit1        PIC *****.
       01 Edit2        PIC *,999.
       01 Edit3        PIC $99999.
       01 Edit4        PIC ******.
       01 Edit5        PIC $999.00.
       01 Edit6        PIC $**9.00.
       01 Edit7        PIC +9999.
       01 Edit8        PIC --99.
       01 Edit9        PIC +++9.

       PROCEDURE DIVISION.
       Begin.
           MOVE 12345 TO Edit1 
           DISPLAY "PIC9(5) 12345 = " Edit1
           MOVE 01234 TO Edit1 
           DISPLAY "PIC9(5) 01234 = " Edit1
           MOVE 00123 TO Edit1 
           DISPLAY "PIC9(5) 00123 = " Edit1 
           MOVE 00012 TO Edit2 
           DISPLAY "PIC9(5) 00012 = " Edit2 
           MOVE 412345 TO Edit3 
           DISPLAY "PIC9(6) 412345 = " Edit3
           MOVE 000123 TO Edit4 
           DISPLAY "PIC9(6) 000123 = " Edit4
           MOVE 000001 TO Edit4
           DISPLAY "PIC9(6) 000001 = " Edit4
           MOVE 000000 TO Edit4
           DISPLAY "PIC9(6) 000000 = " Edit4
           MOVE 0123.45 TO Edit5
           DISPLAY "PIC9(6)V99 012345 = " Edit5
           MOVE 0001.23 TO Edit6
           DISPLAY "PIC9(6)V99 000123 = " Edit6
           MOVE 0000.25 TO Edit6
           DISPLAY "PIC9(6)V99 000025 = " Edit6
           MOVE 000000 TO Edit4
           DISPLAY "PIC9(6)V99 000000 = " Edit4
           MOVE 1234 TO Edit7 
           DISPLAY "PIC9(6)V99 1234 = " Edit7
           MOVE -0012 TO Edit8
           DISPLAY "PIC9(6)V99 -0012 = " Edit8
           MOVE 0004 TO Edit9
           DISPLAY "PIC9(6)V99 0004 = " Edit9
           MOVE 0000 TO Edit4
           DISPLAY "PIC9(6)V99 0000 = " Edit4
           .
       